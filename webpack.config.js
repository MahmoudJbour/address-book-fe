const yargs = require('yargs');
const webpack = require('webpack');
const path = require('path');
const LodashModuleReplacementPlugin = require('lodash-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const options = yargs.alias('p', 'production').argv;

const config = {
  resolve: {
    modules: [
      path.join(__dirname, 'src'),
      'node_modules',
    ],
    extensions: ['.js', '.jsx', '.json', '.scss', '.css', '.sass'],
    alias:{
      'propera': path.resolve(__dirname, './node_modules/@freightos/propera/lib/app/propera'),
      'public': path.resolve('public')
    }
  },

  entry: './src/index.js',

  output: {
    path: path.join(__dirname, 'dist'),
    filename: options.production ? '[name].min.js' : '[name].js',
    library: 'properaReactAddressBook', // TODO: change the name
    libraryTarget: 'umd',
  },

  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        include: path.join(__dirname, 'src'),
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.json$/,
        use: 'json-loader',
      },
      {
        test: /\.css$/i,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
              options: {
                sourceMap: false,
                url: false,
                minimize: true,
              },
            },
            {
              loader: 'sass-loader',
              options: {
                sourceMap: true,
                url: false,
                data: '$env: production;',
              },
            },
          ],
        }),
      },
      {
        test: /\.scss$/i,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
              options: {
                sourceMap: false,
                url: false,
                minimize: true,
              },
            },
            {
              loader: 'sass-loader',
              options: {
                sourceMap: true,
                url: false,
                data: '$env: production;',
              },
            },
          ],
        }),
      },
      {
        test: /\.(svg|eot|ttf|woff|woff2)(\?.+)?$/,
        use: ['file-loader'],
      },
    ],
  },

  plugins: [
    new LodashModuleReplacementPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(options.production ? 'production' : 'development'),
      },
    }),
    new ExtractTextPlugin({
      filename: 'main.css',
      allChunks: true
    })
  ],
};

if (options.production) {
  config.plugins.push(new webpack.optimize.UglifyJsPlugin({ sourceMap: false }));
}

module.exports = config;
