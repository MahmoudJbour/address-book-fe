# Propera React Review #
![alt text](https://cdn.frontify.com/api/screen/thumbnail/8h-SJxKuNIeY4DUEiw3HyA21cpklucjJRDFGhCN_JHPlPSB8dMlr2A5gYEc2jeGt-td1KYk1w45ZVIRz2HL1yw/500 "Freightos Logo")

## What is Feature Flagging? ##
Propera React Review is a module that includes all the components and models related to review microservice.
 
## Getting Started ##
Getting started with Propera React Review should be quite simple

### Installation ###
You can install Feature Flagging using this command
```
npm install --save @freightos/propera-react-review
```

To connect Propera React Review module to your Redux store you'll need the following pieces from the propera-react-review package:

1. Redux Reducer: reviewsReducer.

2. Redux Middleware: reviewsMiddleware

It's important to understand their responsibilities:

1. reviewsReducer: function that tells how to update the Redux store based on changes coming from the application. Those changes are described by Redux actions.

2. reviewsMiddleware: some code you can put between the reducer receive the action, and the dispatcher generating the action.

### Setup

#### Step 1: Reviews Reducer
The store should know how to handle actions coming from the propera react review package. To enable this, we need to pass the reviewsReducer to your store.
```
import { createStore, combineReducers } from 'redux'
import { reducer as reviewsReducer } from '@freightos/propera-react-review'

const rootReducer = combineReducers({
  // ...your other reducers here
  // you have to pass reviewsReducer under 'reviews' key
  reviews: reviewsReducer
})

const store = createStore(rootReducer)
```

#### Step 2: Reviews Saga
The reviews module should know the actions dispatched and handle its actions.
```
import { createStore, combineReducers } from 'redux'
import createSagaMiddleware from 'redux-saga';
import { saga as reviewsSaga } from '@freightos/propera-react-review'

const rootReducer = ...

const sagaMiddleware = createSagaMiddleware();

const rootMiddleware = applyMiddleware({
  // ...your other middlewares here
  sagaMiddleware
})

sagaMiddleware.run(reviewsSaga);

const store = createStore(rootReducer, rootMiddleware)
```

## Usage Guide
To get the reviews value, you need only to use the model
```
import { createStore, combineReducers } from 'redux'
import { Panel, Rating } from '@freightos/propera-react-review'

export const App = () => {
    return(
        <div>
            <Rating revieweeKey={<RevieweeKey>}/>
            <Panel/>
        <div/>
    )
}
...
```
