import { initialize } from '@freightos/propera-translator';

initialize(window.locale, window.jstranslatedStrings);
export { t, formatPlurals, formatHTMLMessage } from '@freightos/propera-translator';
