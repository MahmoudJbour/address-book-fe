import * as Types from './actionTypes';

/**
 * Show the review panel
 * @param revieweeKey The reviewee key
 */
export const showPanel = (revieweeKey, forwarderName, language) => ({
  type: Types.SHOW_PANEL,
  revieweeKey,
  forwarderName,
  language,
});

/**
 * Hide the review panel
 */
export const hidePanel = () => ({
  type: Types.HIDE_PANEL,
});

/**
 * Initialize component
 */
export const initialize = payload => ({
  type: Types.INITIALIZE,
  payload,
});

/**
 * Get the reviewee average rating
 * @param revieweeKey The reviewee key
 */
export const getRevieweeAverageRating = revieweeKey => ({
  type: Types.GET_REVIEWEE_AVERAGE_RATING,
  revieweeKey,
});

/**
 * Get the reviewee reviews
 * @param revieweeKey The reviewee key
 */
export const getRevieweeReviews = revieweeKey => ({
  type: Types.GET_REVIEWEE_REVIEWS,
  revieweeKey,
});

/**
 * Set the reviewee data into the store
 * @param revieweeKey The reviewee key
 * @param data the reviwee data
 */
export const setRevieweeData = (revieweeKey, data) => ({
  type: Types.SET_REVIEWEE_DATA,
  revieweeKey,
  data,
});

/**
 * Gets the seller profile
 * @param {The reviewee/seller key} sellerKey
 */
export const fetchSellerProfile = (sellerKey, language) => ({
  type: Types.FETCH_SELLER_PROFILE,
  sellerKey,
  language,
});

/**
 * Sets seller profile to the store
 * @param {The reviewee/seller key} sellerKey
 */
export const setSellerProfile = (sellerKey, data) => ({
  type: Types.SET_SELLER_PROFILE,
  payload: {
    sellerKey,
    ...data,
  },
});

/**
 * Gets only shipments processed for a seller
 * @param {The Seller Key} sellerKey
 */
export const fetchShipmentsProcessed = sellerKey => ({
  type: Types.FETCH_SHIPMENTS_PROCESSED,
  sellerKey,
});
