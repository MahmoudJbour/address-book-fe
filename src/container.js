import { connect } from 'react-redux';

import AddressBookCard from './components/AddressBookCard';
import {openPopup} from './form/actions';
import {isPopupOpen} from './form/model';

const mapStateToProps = (state, ownProps) =>{
    const popupOpen = isPopupOpen(state);
    console.log("popupOpen", popupOpen)
    return {
        state,
        popupOpen,
        ...ownProps
    }
}

const mapDispatchToProps = (dispatch) =>{
    return {
        openPopupForm: ()=>{
            dispatch(openPopup(true));
        },
        onClose: ()=>{
            dispatch(openPopup(false));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddressBookCard);