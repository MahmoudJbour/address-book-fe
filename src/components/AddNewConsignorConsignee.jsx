import React, { Component } from 'react';
import PropTypes from 'prop-types';

import FButton from 'propera/components/buttons';
import Icons from 'propera/icons';

import { t } from '../translationProvider';

class AddNewConsignorConsignee extends Component {
    render() {
        const { btnLabel, disabled, openPopup } = this.props;
        return (
            <FButton className="add-persona-btn" disabled={disabled} onClick={openPopup}>
                <Icons icon="circle-add" className="add-icon"/>
                <span className="label">{btnLabel}</span>
            </FButton>
        );
    }
}

export default AddNewConsignorConsignee;