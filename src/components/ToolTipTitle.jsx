import React, { Component } from 'react';
import FTooltip from 'propera/components/tooltips';
import Icons from 'propera/icons';

const ToolTipTitle = (title, tooltip, titleClassName) => (
    <label>
        {title}
        <FTooltip
            className={titleClassName}
            attachedTo={
                <span className="inline-logo">
                    <Icons icon="circle-help" />
                </span>
            }
            trigger="hover"
        >
            {tooltip}
        </FTooltip>
    </label>
);

export default ToolTipTitle;