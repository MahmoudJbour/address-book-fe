import React, { Fragment, Component } from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'react-bootstrap';

import FInputField from 'propera/components/form/inputField';
import FCheckBox from 'propera/components/form/checkbox';
import FCard from 'propera/components/widgets/FCard';
import * as Validation from 'propera/forms/validation';

import { CONSIGNOR_CONSIGNEE } from '../constants';
import { t } from '../translationProvider';
import AddNewConsignorConsignee from './AddNewConsignorConsignee';
import PersonaPopup from '../form/components/PersonaPopup';
import ToolTipTitle from './ToolTipTitle'


class AddressBookCard extends Component {
    render() {
        const { title, tooltip, showUSCC, missingUSCCInTheOtherSide,
             cardLabel, btnLabel, personaList = [], name, disabled, openPopupForm } = this.props;
        return (
            <div className="address-book">
                <FCard title={ToolTipTitle(title, tooltip, '')} className={!showUSCC && !missingUSCCInTheOtherSide ? 'missingUscc' : ''}>
                    {
                        personaList && personaList.length == 0 &&
                        <Fragment>
                            <div className="address-book-card-label">{cardLabel}</div>
                            <AddNewConsignorConsignee
                                btnLabel={t(`propera/address-book-${name}`, `Add new ${name}`)}
                                openPopup={openPopupForm}
                                disabled={disabled}
                            />
                            <PersonaPopup {...(this.props)} />
                        </Fragment>
                    }
                </FCard>
            </div>
        );
    }
}

export default AddressBookCard;