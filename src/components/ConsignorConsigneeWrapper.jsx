import React, { Fragment, Component } from 'react';
import { Field } from 'redux-form';
import { Row, Col } from 'react-bootstrap';

import FInputField from 'propera/components/form/inputField';
import FCheckBox from 'propera/components/form/checkbox';
import FTooltip from 'propera/components/tooltips';
import FCard from 'propera/components/widgets/FCard';
import * as Validation from 'propera/forms/validation';
import Icons from 'propera/icons';

import { CONSIGNOR_CONSIGNEE } from '../constants';
import { t } from '../translationProvider';
import AddressBookCard from './AddressBookCard'

const Title = (title, tooltip, titleClassName) => (
    <label>
        {title}
        <FTooltip
            className={titleClassName}
            attachedTo={
                <span className="inline-logo">
                    <Icons icon="circle-help" />
                </span>
            }
            trigger="hover"
        >
            {tooltip}
        </FTooltip>
    </label>
);

const ConsignorConsigneeWrapper = (props) => {
    const {
        title,
        tooltip,
        disabled,
        input: { name },
        shouldShowDeliveryAddressCheckBox,
        showDeliveryAddressSection,
        showUSCC,
        missingUSCCInTheOtherSide,
    } = props;

    return (
        <Fragment >
            // consignor
            <AddressBookCard {...props} cardLabel="label goes here" name="consignor" />
            <AddressBookCard {...props} cardLabel="label goes here" name="consignee" />
        </Fragment>
    );
};

export default ConsignorConsigneeWrapper;
