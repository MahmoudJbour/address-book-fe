import { merge } from 'lodash';
import * as Types from './actionTypes';
import { FIELDS } from './model/reviewState';

const PANEL_MODEL = FIELDS.PROPS.PANEL;

const initialState = {
  [PANEL_MODEL.name]: {
    [PANEL_MODEL.PROPS.IS_VISIBLE]: false,
    [PANEL_MODEL.PROPS.FORWARDER_NAME]: '',
  },

  [FIELDS.PROPS.REVIEWEES]: {},
  [FIELDS.PROPS.SELLER_PROFILES]: {},
};

export default (state = initialState, action) => {
  switch (action.type) {
    case Types.SHOW_PANEL:
    {
      const panel = state[FIELDS.PROPS.PANEL.name];
      const currentReviewee = panel && panel[FIELDS.PROPS.PANEL.PROPS.REVIEWEE_KEY];
      const isCurrentlyVisible = panel && panel[FIELDS.PROPS.PANEL.PROPS.IS_VISIBLE];
      return {
        ...state,
        panel: merge(state.panel, {
          isVisible: !(currentReviewee === action.revieweeKey && isCurrentlyVisible),
          revieweeKey: action.revieweeKey,
          forwarderName: action.forwarderName,
        }),
      };
    }

    case Types.HIDE_PANEL:
      return {
        ...state,
        panel: merge(state.panel, {
          isVisible: false,
        }),
      };

    case Types.SET_REVIEWEE_DATA:
      return {
        ...state,
        reviewees: merge(state.reviewees, {
          [action.revieweeKey]: action.data,
        }),
      };

    case Types.SET_SELLER_PROFILE: {
      const { payload } = action;

      const profiles = state[FIELDS.PROPS.SELLER_PROFILES] || {};
      const { [payload.sellerKey]: profile = {} } = profiles;
      const updatedProfile = { ...profile, ...payload };

      return {
        ...state,
        [FIELDS.PROPS.SELLER_PROFILES]: {
          ...profiles,
          [payload.sellerKey]: updatedProfile,
        },
      };
    }

    default:
      return state;
  }
};
