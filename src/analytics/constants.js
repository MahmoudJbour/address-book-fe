export const NAME = 'analytics';

export const platforms = {
  SEGMENT: 'SEGMENT',
};

export const SEGMENT_EVENTS = {
  seller_profile_homepage_clicked: 'seller_profile_homepage_clicked',
  seller_number_of_reviews_clicked: 'seller_number_of_reviews_clicked',
};
