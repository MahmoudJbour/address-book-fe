import * as Types from './actionTypes';
import { platforms } from './constants';

export const registerEvent = (platform, meta) => ({
  type: Types.REGISTER,
  platform,
  meta,
});

export const registerSegmentEvent = (eventName, properties) =>
  registerEvent(platforms.SEGMENT, { eventName, ...properties });
