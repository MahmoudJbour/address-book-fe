import { takeEvery, takeLatest } from 'redux-saga';
import { put, select, call } from 'redux-saga/effects';
import * as actions from './actions';
import { GET_REVIEWEE_AVERAGE_RATING, GET_REVIEWEE_REVIEWS, SHOW_PANEL, FETCH_SELLER_PROFILE, FETCH_SHIPMENTS_PROCESSED, INITIALIZE } from './actionTypes';
import { getAverageRating, getReviewee, getReviews } from './model';
import { FIELDS } from './model/reviewee';
import { getAverageRatingUrl, getReviewsUrl, getSellerProfileEndPoint, getShipmentsProcessedEndPoint } from './constants';
import { doGet } from './http';
import { getSellerProfileByKey } from './model/profileState';
import { isSellerProfileEmpty } from './utils';

/**
 * Current state selector
 * @param state
 */
export const currentState = state => state;

/**
 * Get the average rating for specific reviewee key
 * @param action
 */
export function* getRevieweeAverageRating(action) {
  try {
    const { revieweeKey } = action;
    // Get the current state
    const state = yield select(currentState);
    // Get the current reviewee data
    const reviewee = getReviewee(state, revieweeKey);
    // Check if the average rating is not exists first
    if (!getAverageRating(reviewee)) {
      // Send request to get average rating
      const { data } = yield call(doGet, getAverageRatingUrl(revieweeKey));
      // Set the returned data in the store
      yield put(actions.setRevieweeData(revieweeKey, data));
    }
  } catch (e) {
    console.error(e);
  }
}

export function* getRevieweeReviews(action) {
  try {
    const { revieweeKey } = action;
    // Get the current state
    const state = yield select(currentState);
    // Get the current reviewee data
    const reviewee = getReviewee(state, revieweeKey);
    // Check if the reviews is not exists first
    if (!getReviews(reviewee)) {
      // Send request to get the reviews
      const { data } = yield call(doGet, getReviewsUrl(revieweeKey));
      // Set the returned data in the store
      yield put(actions.setRevieweeData(revieweeKey, {
        [FIELDS.REVIEWS]: data,
      }));
    }
  } catch (e) {
    console.error(e);
  }
}

export function* showPanel({ revieweeKey, language }) {
  yield put(actions.initialize({ revieweeKey, language }));
}

export function* fetchSellerProfile(action) {
  const { sellerKey, language } = action;
  const state = yield select(currentState);
  const profile = getSellerProfileByKey(state, sellerKey);

  if (!isSellerProfileEmpty(profile)) return;

  try {
    const { data: response } = yield call(
      doGet,
      getSellerProfileEndPoint(sellerKey, language),
      undefined,
      true,
    );

    yield put(actions.setSellerProfile(sellerKey, response));
  } catch (e) {
    console.error(e);
  }
}

export function* fetchShipmentsProcessed(action) {
  const { sellerKey } = action;
  const state = yield select(currentState);

  const profile = getSellerProfileByKey(state, sellerKey);

  if (profile.shipmentsProcessed !== undefined) return;

  try {
    const { data: shipmentsProcessed } = yield call(
      doGet,
      getShipmentsProcessedEndPoint(sellerKey),
      undefined,
      true,
    );

    yield put(actions.setSellerProfile(sellerKey, { shipmentsProcessed }));
  } catch (e) {
    console.error(e);
  }
}

export function* initialize(action) {
  const { payload: { revieweeKey, language } } = action;

  yield put(actions.fetchSellerProfile(revieweeKey, language));
  yield put(actions.getRevieweeAverageRating(revieweeKey));
  yield put(actions.getRevieweeReviews(revieweeKey));
}

/**
 * Main saga watcher
 */
export default function* watcher() {
  yield [
    takeEvery(GET_REVIEWEE_REVIEWS, getRevieweeReviews),
    takeEvery(GET_REVIEWEE_AVERAGE_RATING, getRevieweeAverageRating),
    takeEvery(FETCH_SHIPMENTS_PROCESSED, fetchShipmentsProcessed),
    takeEvery(FETCH_SELLER_PROFILE, fetchSellerProfile),
    takeLatest(SHOW_PANEL, showPanel),
    takeLatest(INITIALIZE, initialize),
  ];
}
