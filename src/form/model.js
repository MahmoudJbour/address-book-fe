
import {FORM_NAME} from './constants'

export const isPopupOpen = (state) => {
    try {
      return state.freightos[FORM_NAME].isPopupOpen;
    } catch (error) {
      return false;
    }
  };