import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import { Row, Col } from 'react-bootstrap';

import { CONSIGNOR_CONSIGNEE, FORM_NAME } from '../../constants';
import { t } from 'propera/translations/translationProvider';
import { validation as Validation } from 'propera/forms';
import FCheckbox from 'propera/components/form/checkbox/FCheckbox';
import FModal from 'propera/components/communication/FModal';
import FInputField from 'propera/components/form/inputField';
import ToolTipTitle from '../../components/ToolTipTitle'

class PersonaPopup extends Component {
    render() {
        const {
            disabled,
            showUSCC,
            order,
            name,
            popupOpen,
            onClose
        } = this.props;

        return (
            <FModal
                title={<div style={{color:"red"}}>Test inside shit</div>}
                isOpen={popupOpen}
                className="persona-popup"
                closable
                hideOnClickOutside
                onClose={onClose}
            >
                <form>
                    <Row className="no-gutter">
                        <Col sm={4}>
                            <Field
                                component={FInputField}
                                name={`${name}.${CONSIGNOR_CONSIGNEE.COMPANY.NAME}`}
                                type="text"
                                label={t('shipment/Company', 'Company')}
                                disabled={disabled}
                                tabIndex={order}
                                validate={[Validation.required]}
                                autoFocus={name === CONSIGNOR_CONSIGNEE.COMPANY.NAME}
                            />
                        </Col>
                        <Col sm={4}>
                            <Field
                                component={FInputField}
                                name={`${name}.${CONSIGNOR_CONSIGNEE.NAME.NAME}`}
                                type="text"
                                label={t('freightOrder/IssueBindingPurchaseOrderForShippingMP/Full name', 'Full name')}
                                disabled={disabled}
                                tabIndex={order + 1}
                                validate={[Validation.required]}
                                maxLength={200}
                            />
                        </Col>
                    </Row>
                    <Row className="no-gutter">
                        <Col sm={4}>
                            <Field
                                component={FInputField}
                                name={`${name}.${CONSIGNOR_CONSIGNEE.ADDRESS.NAME}`}
                                type="text"
                                label={t('shipment/Address', 'Address')}
                                disabled={disabled}
                                tabIndex={order + 2}
                                validate={[Validation.required]}
                                maxLength={250}
                            />
                        </Col>
                        <Col sm={4}>
                            <Field
                                component={FInputField}
                                name={`${name}.${CONSIGNOR_CONSIGNEE.EMAIL.NAME}`}
                                type="email"
                                label={t('common/Email', 'Email')}
                                disabled={disabled}
                                tabIndex={order + 3}
                                validate={[Validation.required, Validation.email]}
                            />
                        </Col>
                    </Row>

                    <Row className="no-gutter">
                        <Col sm={4}>
                            <Field
                                component={FInputField}
                                name={`${name}.${CONSIGNOR_CONSIGNEE.EMAIL.NAME}`}
                                type="email"
                                label={t('common/Email', 'Email')}
                                disabled={disabled}
                                tabIndex={order + 3}
                                validate={[Validation.required, Validation.email]}
                            />
                        </Col>
                        <Col sm={4}>
                            <Field
                                component={FInputField}
                                name={`${name}.${CONSIGNOR_CONSIGNEE.PHONE.NAME}`}
                                type="text"
                                label={t('common/Phone', 'Phone')}
                                disabled={disabled}
                                tabIndex={order + 4}
                                validate={[Validation.required]}
                            />
                        </Col>
                    </Row>
                </form>
            </FModal>
        );

    }
}

export default PersonaPopup;