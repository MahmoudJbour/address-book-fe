import * as Types from './actionTypes';

export const openPopup = isOpen => ({
  type: Types.IS_OPEN_POPUP,
  isOpen,
});