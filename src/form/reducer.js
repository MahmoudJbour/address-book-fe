import * as Types from './actionTypes';

const initialState = {
  isPopupOpen: false,
};

export default (state = initialState, action) => {
  const { type, ...rest } = action;

  switch (type) {
    case Types.IS_OPEN_POPUP:
      const newState = JSON.parse(JSON.stringify(state));
      newState.isPopupOpen = rest.isOpen;
      return newState;
    default:
      return state;
  }
};