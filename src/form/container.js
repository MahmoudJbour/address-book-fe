import { connect } from 'react-redux';
import { reduxForm, touch, initialize, formValueSelector, getFormValues } from 'redux-form';

import PersonaPopup from './components/PersonaPopup';
import {FORM_NAME} from './constants'
import {isPopupOpen} from './model';

const mapStateToProps = (state) =>{
    // const isPopupOpen = isPopupOpen(state);
    return {
        state
    }
}

const mapDispatchToProps = (dispatch) =>{
    return {

    }
}
const personaForm = reduxForm({
    form: FORM_NAME,
})(PersonaPopup);

export const personaFormContainer = connect(mapStateToProps, mapDispatchToProps)(personaForm);