import axios from 'axios';
import { getUrl } from './utils';

/**
 * Send HTTP GET request to Review Service
 * @param url the requested url
 * @param headers the request headers
 */
export const doGet = (url, headers = {}, forSeller = false) =>
  axios.get(url, {
    baseURL: getUrl(undefined, forSeller),
    headers: {
      'Content-Type': 'application/json',
      ...headers,
    },
    data: {},
  });

