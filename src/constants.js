import { t } from './translationProvider';

export const NAME = '@@addressBook';
export const FORM_NAME = 'persona';

// local url indicator
export const LOCAL = 'local';
export const TEST = 'test';
// local machine url
export const LOCAL_PORT = 9006;
export const LOCAL_SELLER_SERVICE_PORT = 9004;

export const IconsList = {
  close: 'close',
  circleCheck: 'circle-check',
  circleBell: 'circle-bell',
};

/** **************************************
 *               API
 *************************************** */
export const HOSTNAME = {
  LOCAL: 'flux.freightos.local',
  TEST: 'flux.test-freightos.com',
  PRODUCTION: 'flux.freightos.com',
};

export const CONSIGNOR_CONSIGNEE = {
  NAME: {
    NAME: 'name',
    DV: '',
  },
  COMPANY: {
    NAME: 'company',
    DV: '',
  },
  ADDRESS: {
    NAME: 'address',
    DV: '',
  },
  EMAIL: {
    NAME: 'email',
    DV: '',
  },
  PHONE: {
    NAME: 'phone',
    DV: '',
  },
  DELIVERY_ADDRESS_FIELD: {
    NAME: 'showDeliveryAddress',
    DV: false,
  },
  USCC: {
    NAME: 'uscc',
    DV: '',
  },
};

export const CONTACT_AND_ADDRESS_FIELDS = {
  NAME: {
    NAME: 'fullName',
    DV: '',
  },
  COMPANY: {
    NAME: 'companyName',
    DV: '',
  },
  ADDRESS: {
    NAME: 'address',
    DV: '',
  },
  EMAIL: {
    NAME: 'email',
    DV: '',
  },
  PHONE: {
    NAME: 'phone',
    DV: '',
  },
  KEY: {
    NAME: 'key',
    DV: '',
  },
};


export const getAverageRatingUrl = revieweeKey =>
  `/api/v1/reviews/reviewee/${revieweeKey}/averageRating`;
export const getReviewsUrl = revieweeKey =>
  `/api/v1/reviews/reviewee/${revieweeKey}/reviews`;
export const getSellerProfileEndPoint = (sellerKey, language) => {
  const query = language ? `?language=${language}` : '';
  return `api/v1/seller-profiles/profiles/${sellerKey}${query}`;
};
export const getShipmentsProcessedEndPoint = sellerKey =>
  `api/v1/seller-profiles/profiles/${sellerKey}/shipments-processed`;
