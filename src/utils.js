import moment from 'moment';
import { includes, startsWith, isEmpty } from 'lodash';
import { HOSTNAME, LOCAL, TEST, LOCAL_PORT, LOCAL_SELLER_SERVICE_PORT, ShipmentProcessedLabel } from './constants';

/**
 * Get the host name according to the environment
 * @param fullDomain
 * @returns {string}
 */
export const getHostname = (fullDomain) => {
  const isLocal = includes(fullDomain, LOCAL);
  const isTest = includes(fullDomain, TEST);

  if (isLocal) {
    return HOSTNAME.LOCAL;
  } else if (isTest) {
    return HOSTNAME.TEST;
  }
  return HOSTNAME.PRODUCTION;
};

/**
 * Get the Review/Seller-Profile service Url with port (if local env)
 * @returns {string}
 */
export const getUrl = (domain = window.location.hostname, forSeller) => {
  const isLocal = includes(domain, LOCAL);
  const localPort = forSeller ? LOCAL_SELLER_SERVICE_PORT : LOCAL_PORT;

  return `${isLocal ? 'http' : 'https'}://${getHostname(domain)}${isLocal ? `:${localPort}` : ''}`;
};


/**
 * Format the submitted date
 * @param submittedDate The data in UNIX form
 */
export const formatSubmittedDate = submittedDate =>
  moment(submittedDate).format('MMM YYYY');

/**
 * Adds http to an incomplet URL
 * @param {full or partial URL} url
 */
export const getCompleteUrl = (url) => {
  const lowerCasedUrl = url.toLowerCase();
  if (startsWith(lowerCasedUrl, 'http') || startsWith(lowerCasedUrl, 'https')) {
    return url;
  }
  return `http://${url}`;
};

/**
 * Gets shipment processed label based on total number of shipments processed.
 * @param {Total Number of shipments processed by a seller} numberOfShipments
 */
export const getShipmentProcessedLabel = (numberOfShipments) => {
  if (numberOfShipments < 10) {
    return ShipmentProcessedLabel.NEW;
  } else if (numberOfShipments >= 10 && numberOfShipments < 50) {
    return ShipmentProcessedLabel.TEN_PLUS;
  } else if (numberOfShipments >= 50 && numberOfShipments < 100) {
    return ShipmentProcessedLabel.FIFTY_PLUS;
  } else if (numberOfShipments >= 100 && numberOfShipments < 250) {
    return ShipmentProcessedLabel.HUNDRED_PLUS;
  } else if (numberOfShipments >= 250 && numberOfShipments < 500) {
    return ShipmentProcessedLabel.TWO_FIFTY_PLUS;
  } else if (numberOfShipments >= 500 && numberOfShipments < 750) {
    return ShipmentProcessedLabel.FIVE_HUNDRED_PLUS;
  } else if (numberOfShipments >= 750 && numberOfShipments < 1000) {
    return ShipmentProcessedLabel.SEVEN_FIFTY_PLUS;
  } else if (numberOfShipments >= 1000) {
    return ShipmentProcessedLabel.THOUSAND_PLUS;
  }

  return null;
};


export const isSellerProfileEmpty = (profile = {}) => {
  const { sellerKey, shipmentsProcessed, ...profileData } = profile;

  return isEmpty(profileData);
};

export const stripUrl = (uri = '') =>
  uri.replace(/^(?:https?:\/\/)?(?:www\.)?/i, '').replace(/\/$/, '');
