require('raf/polyfill');
const { initialize } = require('@freightos/propera-translator');

initialize();

//Configure enzyme adapter
const  configure = require('enzyme').configure;
const Adapter = require( 'enzyme-adapter-react-16');
configure({ adapter: new Adapter() });